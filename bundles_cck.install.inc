<?php

function content_bundle_management( $op, $bundle, $meta = array() ){
  switch($op){
    case "install":
      $function = "{$bundle}_install_content";
      if(function_exists($function)){
        $content_types = $function();
        reset($content_types);
        while( list(,$content_type) = each($content_types) ){
          if( __content_create_type($content_type) !== false ){
            bundles_management_add_schema( 'content', $bundle, $content_type['type']['type'] );
          }
        }
      }
      return false;
     break;
    case "uninstall":
      if( array_key_exists( 'schema_items' , $meta ) ){
        $schema_items = $meta['schema_items'];
        
        if( count($schema_items) ){
          content_enable();
        }
        
        reset($schema_items);
        while( list(,$schema_item) = each($schema_items) ){
          $info = new stdClass();
          $info->type = $schema_item['index'];
          content_type_delete($info);
          node_type_delete($schema_item['index']);
          bundles_management_delete_schema( 'content', $bundle, $schema_item['index'] );
        } 
          
        if( count($schema_items) ){
          node_types_rebuild();
          menu_rebuild();
        }

      }
     break;
  }
}

function bundles_get_cck_install_errors(){
  return bundles_set_install_error( array( 'type' => 'cck' ));
}

function bundles_set_cck_install_errors( $message ){
  return bundles_set_install_error( array( 'message' => $message, 'type' => 'cck' ));
}

function __content_create_type( $content ){
 // Get the content type we are importing into.
  $type_label = node_get_types('name', $content['type']['name']);

  // Get all type and field info for this database.
  $content_info = _content_type_info();

  $imported_type = $content['type'];
  $imported_type_name = $imported_type['type'];
  $imported_type_label = $imported_type['name'];

  // It is allowed to import a type with no fields,
  // so the fields array could be empty and must be cast as an array.
  $imported_fields = (array) $content['fields'];

  // Perform more pre-import error trapping.
  // If there are potential problems, exit without doing the import.
  $not_enabled = array();

  // The groups array could be empty and still valid, make sure to cast it as an array.
  // If there are groups in the import, make sure the fieldgroup module is enabled.
  if (module_exists('fieldgroup')) {
    $imported_groups = (array) $content['groups'];
  }
  elseif (is_array($content['groups'])) {
    $not_enabled[] = 'fieldgroup';
  }

  // Make sure that all the field and widget modules in the import are enabled in this database.
  foreach ($imported_fields as $field) {
    if (empty($field['module'])) {
      $not_enabled[] = $field['field_name'];
    }
    else {
      $modules = explode(', ', $field['module']);
      foreach ($modules as $module) {
        if (!module_exists($module)) {
          $not_enabled[] = $module;
        }
      }
    }
  }

  // If any required module is not enabled, set an error message and exit.
  if ($not_enabled) {
    bundles_set_cck_install_errors( t('The following modules must be enabled for this import to work: %modules.', array(
        '%modules' => implode(', ', array_unique($not_enabled))
        ))
    );
  }

  // Make sure the imported content type doesn't already exist in the database.
  if (in_array($imported_type_name, array_keys($content_info['content types']))) {
    bundles_set_cck_install_errors( t('The content type %type already exists in this database.', array(
          '%type' => $imported_type_name
          )));
  }

  if (bundles_get_cck_install_errors()) {
    return false;
  }

  $node_type = (object) $imported_type;
  $node_type->has_title = ($node_type->title_label != '');
  $node_type->has_body = ($node_type->body_label != '');
  $node_type->module = 'node';
  $node_type->custom = true;
  $node_type->modified = TRUE;
  $node_type->locked = false;

  $status = node_type_save($node_type);
  node_types_rebuild();
  // menu_rebuild clears the cache, too
  menu_rebuild();

  // Reset type and database values once new type has been added.
  $type_name  = $imported_type_name;
  $type_label = node_get_types('name', $type_name);
  $content_info = _content_type_info();

  if (bundles_get_cck_install_errors() || !is_array($content_info['content types'][$type_name])) {
      return false;
  }

  // Create the groups for this type, if they don't already exist.
  if (module_exists('fieldgroup')) {
    $groups = fieldgroup_groups($type_name);
    $content_type = content_types($type_name);
    $errors = FALSE;

    foreach ($imported_groups as $group) {
      $group_name = $group['group_name'];
      if (!is_array($groups[$group_name])) {
        $values = (array) $group;
        drupal_execute('fieldgroup_edit_group_form', $values, $content_type, $group_name, 'add');
      }
    }
    cache_clear_all('fieldgroup_data', 'cache_content');
    // Reset the static variable in fieldgroup_groups() with new data.
    fieldgroup_groups('', FALSE, TRUE);
  }

  $should_clear_type_cache = 0;
  // Iterate through the field forms in the import and execute each.
  foreach ($imported_fields as $field) {

    // Make sure the field doesn't already exist in the type.
    // If so, do nothing, fields can't be duplicated within a content type.
    $field_name   = $field['field_name'];
    $field_values = $field;
    $field_label  = $field_values['label'];

    if (!empty($field['field_name']) && is_array($content_info['content types'][$type_name]['fields'][$field_name])) {
      bundles_set_cck_install_errors(t("The imported field %field_label (%field_name) was not added to %type because that field already exists in %type.", array(
          '%field_label' => $field_label,
          '%field_name' => $field_name,
          '%type' => $type_name
          )));
    }
    else {

      $values = array();
      $errors = FALSE;

      // Check if field already exists in the database.
      // If not, add new, otherwise, add existing.

      if (!array_key_exists($field_name, $content_info['fields'])) {
        $values['field_name']        = $field_values['field_name'];
        $values['label']             = $field_values['label'];
        $values['field_widget_type'] = $field_values['field_type'] .'-'. $field_values['widget_type'];
        $values['type_name']         = $type_name;
        $field_url  = drupal_execute('_content_admin_field_add_new', $values, $type_name, $field_name);
        if (bundles_get_cck_install_errors()) {
          bundles_set_cck_install_errors(t("An error has occured adding the field %field_label (%field_name).<br/>Please check the errors displayed for more details.", array(
              '%field_label' => $field_label,
              '%field_name' => $field_name
              )));
          $errors = TRUE;
        }
        else {
          // Retrieve the new field name by picking out the arg from the url returned by
          // _content_admin_field_add_new(), needed so the right field can be updated in the next step.
          $args = explode('/', $field_url);
          $field_name = array_pop($args);
          $field_values['field_name'] = $field_name;
        }
      }
      else {
        $values['field_name']        = $field_values['field_name'];
        $values['type_name']         = $type_name;
        drupal_execute('_content_admin_field_add_existing', $values, $type_name);
        if (bundles_get_cck_install_errors()) {
          bundles_set_cck_install_errors(t("An error has occured adding the field %field_label (%field_name).<br/>Please check the errors displayed for more details.", array(
              '%field_label' => $field_label,
              '%field_name' => $field_name
              )));
          $errors = TRUE;
        }
      }

      // Once the field has been added, update the settings with the macro values.
      // Replace the export type name with the import type name.
      if (!$errors) {
        $field_values['type_name']   = $type_name;
        drupal_execute('_content_admin_field', $field_values, $type_name, $field_name);
        if (bundles_get_cck_install_errors()) {
           bundles_set_cck_install_errors(t("The field %field_label (%field_name) was added to the content type %type, but an error has occured updating the field settings.<br/>Please check the errors displayed for more details.", array(
               '%field_label' => $field_label,
               '%field_name' => $field_name,
               '%type' => $type_label
               )));
        }
        else {
          // If we successfully updated the form, serialize the display_settings info
          // and insert the display_settings (if it exists) into the database.
          if (array_key_exists('display_settings', $field)) {
            $query = 'UPDATE {node_field_instance} SET display_settings = \'%s\' WHERE field_name = \'%s\'';
            db_query($query, serialize($field['display_settings']), $field['field_name']);
            if ($db_err = db_error()) {
              bundles_set_cck_install_errors(t("The field %field_label (%field_name) was added to the content type %type, but an error has occured updating the field's 'display_settings'.<br/>The db error is: '%db_err'.", array(
                   '%field_label' => $field_label,
                   '%field_name' => $field_name,
                   '%type' => $type_label,
                   '%db_err' => $db_err
              )));
            }
            else {
              // If any data was successfully inserted into the DB,
              // we make sure the cache is cleared.
              $should_clear_type_cache = 1;
            }
          }
        }
      }
    }
  }

  // If no errors occured when inserting field data directly into the DB,
  // clear the type cache.
  if ($should_clear_type_cache) {
    content_clear_type_cache();
  }

  if (bundles_get_cck_install_errors()) {
    return false;
  }
}

<?php

function bundle_install_modules( $validaton_modules, $form_values ){
  include_once './includes/install.inc';
  $new_modules = array();

  // Check values for dependency that we can't install.
  if ($dependencies = system_module_build_dependencies($validaton_modules, $form_values)) {
    // These are the modules that depend on existing modules.
    foreach (array_keys($dependencies) as $name) {
      $form_values['status'][$name] = 0;
    }
  }

  $enable_modules = array();
  foreach ($form_values['status'] as $key => $choice) {
    if ($choice) {
      if (drupal_get_installed_schema_version($key) == SCHEMA_UNINSTALLED) {
        $new_modules[] = $key;
      }
      else {
        $enable_modules[] = $key;
      }
    }
  }

  if (!empty($enable_modules)) {
    module_enable($enable_modules);
  }

  $install_modules = array( 'modules' => array(), 'weights' => array() );
  foreach ($new_modules as $key => $modulename) {
    $weight = 0; $dependency_weight = 0;
    if( array_key_exists( $modulename, $install_modules['modules'] )){
      $weight = $install_modules['modules'][$modulename];
    }

    $module = $validaton_modules[$modulename];
    if( is_array( $module->info['dependencies'] ) ){
      $dependencies = $module->info['dependencies'];
      reset($dependencies);
      while( list(,$dependency) = each($dependencies) ){
        if(in_array($dependency,$new_modules)){
          $dependency_weight = $weight-1;
          if( array_key_exists( $dependency, $install_modules['modules'] )){
            $dependency_weight = $install_modules['modules'][$dependency];
            if($dependency_weight > $weight){
              if( in_array($dependency, $install_modules['weights'][$dependency_weight]) ){
                $key = array_search($dependency, $install_modules['weights'][$dependency_weight]);
                unset($install_modules['weights'][$dependency_weight][$key]);
              }
              $dependency_weight = $weight-1;
            }
          }
          $install_modules['modules'][$dependency] = $dependency_weight; 
          $install_modules['weights'][$dependency_weight][] = $dependency; 
        }
      }
    }
    if( array_key_exists( $modulename, $install_modules['modules'] )){
      $existing_weight = $install_modules['modules'][$modulename];
      if( in_array($modulename, $install_modules['weights'][$existing_weight]) ){
        $key = array_search($modulename, $install_modules['weights'][$existing_weight]);
        unset($install_modules['weights'][$existing_weight][$key]);
      }
    }
    $install_modules['modules'][$modulename] = $weight; 
    $install_modules['weights'][$weight][] = $modulename;
  }


  ksort($install_modules['weights']);
  reset($install_modules['weights']);
  while( list(,$modules) = each($install_modules['weights']) ){
    $new_modules = array();
    // Install new modules.
    foreach ($modules as $key => $modulename) {
      if (drupal_check_module($modulename)) {
        $new_modules[] = $modulename;
      }
    }
    drupal_install_modules($new_modules);
  }

  module_list(TRUE, FALSE);
}

function bundles_system_bundle_install($bundle_list = array()) {
  $enable_bundles = array();

  foreach ($bundle_list as $bundle) {
    if (bundles_get_installed_schema_version($bundle, TRUE) == SCHEMA_UNINSTALLED) {
      bundle_load_install($bundle);
      module_invoke($bundle, 'install');
      
      $versions = drupal_get_schema_versions($bundle);
      $version = ( ($versions) ? max($versions) : SCHEMA_INSTALLED);
      drupal_set_installed_schema_version($bundle, $version);
      
      $module_list = module_implements("bundle_management");
      reset( $module_list );
      while( list(,$module) = each($module_list) ){
        $function = "{$module}_bundle_management";
        $function( "install", $bundle, array( 'version' => $version, 'schema_items' => bundles_management_get_schema_items($bundle) ) );
      }
      
      $enable_bundles[] = $bundle;
    }
  }
  bundle_enable($enable_bundles);
}

function bundles_get_install_files($bundle_list = array()) {
  $installs = array();
  foreach ($bundle_list as $bundle) {
    $installs = array_merge($installs, drupal_system_listing($bundle .'.install$', 'bundles'));
  }
  return $installs;
}

function bundle_check_bundle($bundle) {
  $return_value = TRUE;
  // Include install file
  if( is_object($bundle) ){
    if( is_array( $bundle->info['module dependencies'] ) ){
      $modules = $bundle->info['module dependencies'];
      reset($modules);
      while( list(,$module) = each($modules) ){
        if( !module_exists($module) ){
          $return_value = FALSE;
          break;
        }
      }
    }
    if( $return_value ){
      $bundlename = $bundle->name;
      if (isset($install[$bundlename])) {
        $install = bundles_get_install_files(array($bundlename));
        require_once $install[$bundlename]->filename;

        // Check requirements
        $requirements = module_invoke($bundlename, 'requirements', 'install');
        if (is_array($requirements) && drupal_requirements_severity($requirements) == REQUIREMENT_ERROR) {
          // Print any error messages
          foreach ($requirements as $requirement) {
            if (isset($requirement['severity']) && $requirement['severity'] == REQUIREMENT_ERROR) {
              drupal_set_message($requirement['description'] .' ('. t('Currently using !item !version', array('!item' => $requirement['title'], '!version' => $requirement['value'])) .')', 'error');
            }
          }
          $return_value = FALSE;
        }
      }
    }
  }
  return $return_value;
}

function bundles_get_bundle_info( $bundle ){
  $bundles = bundles_set_bundle_info( );
  if( array_key_exists( $bundle, $bundles ) ){
    return $bundles[$bundle];
  }
  return array();
}

function bundles_set_bundle_info( $bundle = null, $info = null ){
  static $bundles = array();
  if( !is_null($bundle) && !is_null($info) ){
    return $bundles;
  }
  else if( !is_null($bundle) ){
    return $bundles[$bundle];
  }
  else {
    $bundles[$bundle] = $info;
  }
}

function bundles_install_build_bundle_dependencies($bundles, $values = null) {
  static $dependencies;

  if (!isset($dependencies) && isset($values)) {
    $dependencies = array( );
    foreach ($bundles as $name => $bundle) {
      // If the module is disabled, will be switched on and it has dependencies.
      if (!$bundle->status && $values['bundle_status'][$name] ) {
        if( isset($bundle->info['bundle dependencies']) ){
          foreach ($bundle->info['bundle dependencies'] as $dependency) {
            if (!$values['bundle_status'][$dependency] && isset($bundles[$dependency])) {
              if (!isset($dependencies[$name])) {
                $dependencies[$name] = array();
              }
              $dependencies[$name][] = $dependency;
            }
          }
        }
      }
    }
  }
  return $dependencies;
}

function bundles_install_build_module_dependencies($bundles, $modules, $form_values = null) {
  static $dependencies;
  if (!isset($dependencies) && is_array($form_values) && array_key_exists( 'bundle_status', $form_values ) &&  array_key_exists( 'status', $form_values ) ) {
    $bundle_values = $form_values['bundle_status'];
    $dependencies = array( );
    $current_values = array();
    reset($modules); while( list($modulename,$module) = each($modules) ){  $current_values['status'][$modulename] = $module->status;  }
    foreach ($bundles as $name => $bundle) {
      // If the bundle is disabled, will be switched on and it has dependencies.
      if (!$bundle->status && $bundle_values[$name] ) {
        if( isset($bundle->info['module dependencies']) ){
          foreach ($bundle->info['module dependencies'] as $dependency) {
            if (!isset($dependencies[$name])) {
              $dependencies[$name] = array();
            }
            $module_values = $current_values;
            $module_values['status'][$dependency] = 1;
            $module_dependencies = system_module_build_dependencies($modules, $module_values);
            if( is_array($module_dependencies[$dependency]) )
              $dependencies[$name] = $module_dependencies[$dependency];
            if (!$form_values['status'][$dependency] && isset($modules[$dependency])) {
              $dependencies[$name][] = $dependency;
            }
          }
        }
        if( !count($dependencies[$name]) )
          unset($dependencies[$name]);
      }
    }
  }
  return $dependencies;
}

function bundles_install_bundles_form( $form_values = null ){
  $form = array();

  $bundles = bundles_rebuild_cache();
  $modules = _bundles_get_all_modules();

  if( isset( $form_values ) ){
    if( bundles_install_build_bundle_dependencies( $bundles, $form_values ) ){
      return bundles_install_bundles_confirm_bundles_form( $bundles, $modules, $form_values );
    }
    if( $module_form = bundles_install_bundles_confirm_modules_form( $bundles, $modules, $form_values ) ){
      return $module_form;
    }
  }
  $form['validation_modules'] = array( '#type' => 'value', '#value' => $modules );
  $form['validation_bundles'] = array( '#type' => 'value', '#value' => $bundles );

  $module_status = array();
  reset($modules);
  while( list($modulename, $module) = each($modules) ){
    $status = $module->status;
    if( is_array($form_values['status']) && array_key_exists($modulename, $form_values['status']  ) ) $status = $form_values['status'][$modulename];
    $module_status[$modulename] = $status;
  }

  $next = 0;

  $disabled = array();
  $options = array(); $status = array();
  reset($bundles);
  while( list($bundlename,$bundle) = each($bundles) ){
    $form['name'][$bundlename] = array('#value' => $bundle->info['name']);
    $form['version'][$bundlename] = array('#value' => $bundle->info['version']);
    $form['description'][$bundlename] = array('#value' => t($bundle->info['description']));
    $options[$bundlename] = '';
    if ($bundle->status) {
      $status[] = $bundle->name;
    }

    $dependencies = array();
    // Check for missing dependencies.
    if (is_array($bundle->info['module dependencies'])) {
      foreach ($bundle->info['module dependencies'] as $dependency) {
        if ( !array_key_exists( $dependency, $modules ) ) {
          $dependencies[] = drupal_ucfirst($dependency) . t(' (<span class="admin-missing">missing</span>)');
          $disabled[] = $bundlename;
        }
        else if( !module_exists($dependency) ){
          $module = $modules[$dependency];
          if( is_array( $module->info['dependencies'] ) ){
              reset($module->info['dependencies']);
              while( list( ,$module_dependency ) = each($module->info['dependencies']) ){
                if( !array_key_exists($module_dependency, $dependencies) && !module_exists($module_dependency) )
                  $dependencies[$module_dependency] = $modules[$module_dependency]->info['name'] . t(' (<span class="admin-missing">disabled</span>)');
              }
          }
          if( !array_key_exists($dependency, $dependencies) )
            $dependencies[$dependency] = $modules[$dependency]->info['name'] . t(' (<span class="admin-missing">disabled</span>)');
        }
      }

      // Add text for dependencies.
      if (!empty($dependencies)) {
        $form['description'][$bundlename]['dependencies'] = array(
          '#value' => t('Depends on: !dependencies', array('!dependencies' => implode(', ', $dependencies))),
          '#prefix' => '<div class="admin-dependencies">',
          '#suffix' => '</div>',
        );
      }
    }

  }

  // Handle status checkboxes, including overriding
  // the generated checkboxes for required bundles.
  $form['bundle_status'] = array(
    '#type' => 'checkboxes',
    '#default_value' => $status,
    '#options' => $options,
    '#process' => array(
      'expand_checkboxes' => array(),
      'system_modules_disable' => array($disabled),
    ),
  );

  $form['status'] = array(
    '#type'  => 'hidden_array',
    '#values' => $module_status,
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['step'] = array( '#type' => 'hidden', '#value' => 'install' );
  $form['#multistep'] = TRUE;
  $form['#action'] = url( 'admin/build/bundles/install/confirm' );

  return $form;
}

function bundles_install_bundles_submit( $form_id, $form_values ){
  $bundles = $form_values['validation_bundles'];
  $modules = $form_values['validation_modules'];

  // Check values for dependency that we can't install.
  if ($bundle_dependencies = bundles_install_build_bundle_dependencies($bundles, $form_values)) {
    // These are the modules that depend on existing bundles.
    reset($bundle_dependencies);
    while ( list($name, $bundle) = each($bundle_dependencies) ) {
      $form_values['bundle_status'][$name] = 0;
    }
  }
  
  if ($module_dependencies = bundles_install_build_module_dependencies($bundles, $modules, $form_values)) {
    // These are the modules that depend on existing modules.
    reset($module_dependencies);
    while ( list($bundle,$module_list) = each($module_dependencies) ) {
      $form_values['bundle_status'][$bundle] = 0;
      reset($module_list);
      while( list(,$module) = each($module_list) ){
        $form_values['status'][$module] = 0;
      }
    }
  }

  bundle_install_modules($modules,$form_values);
  $new_bundles = array();
  $enable_bundles = array();
  $disable_bundles = array();
  foreach ($form_values['bundle_status'] as $key => $choice) {
    if ($choice) {
      if (bundles_get_installed_schema_version($key) == SCHEMA_UNINSTALLED) {
        $new_bundles[] = $key;
      }
      else {
        $enable_bundles[] = $key;
      }
    }
    else {
      $disable_bundles[] = $key;
    }
  }

  // Install new modules.
  foreach ($new_bundles as $key => $bundle) {
    if (!bundle_check_bundle($bundles[$bundle])) {
      unset($new_bundles[$key]);
    }
  }

  $old_bundle_list = bundle_list();

  if (!empty($enable_bundles)) {
    bundle_enable($enable_bundles);
  }
  if (!empty($disable_bundles)) {
    bundle_disable($disable_bundles);
  }

  bundles_system_bundle_install($new_bundles);

  $current_bundle_list = bundle_list(TRUE);

  if ($old_bundle_list != $current_bundle_list) {
    menu_rebuild();
    node_types_rebuild();
    drupal_set_message(t('The bundle configuration options have been saved.'));
  }

  // If there where unmet dependencies and they haven't confirmed don't redirect.
  if ( $bundle_dependencies && !isset($form_values['confirm'])) {
    return false;
  }
  // If there where unmet dependencies and they haven't confirmed don't redirect.
  if ( $module_dependencies && $form_values['step'] != "confirm/modules" ) {
    return false;
  }

  drupal_clear_css_cache();

  return 'admin/build/bundles/install';
}

function theme_bundles_install_bundles( $form ){
  $output = "";

  // Individual table headers.
  $header = array(t('Enabled'));
  $header[] = t('Name');
  $header[] = t('Version');
  $header[] = t('Description');

  // Pull package information from module list and start grouping modules.
  $bundles = $form['validation_bundles']['#value'];

  // Display packages.
  $output = '';
  $rows = array();
  foreach ($bundles as $bundlename => $bundle) {
    $row = array();
    $row[] = array('data' => drupal_render($form['bundle_status'][$bundlename]), 'align' => 'center');

    $row[] = '<strong>'. drupal_render($form['name'][$bundlename]) .'</strong>';
    $row[] = drupal_render($form['version'][$bundlename]);
    $row[] = array('data' => drupal_render($form['description'][$bundlename]), 'class' => 'description');
    $rows[] = $row;
  }
  $output .= theme('table', $header, $rows, array('class' => 'bundles'));

  $output .= drupal_render($form);
  return $output;
}

function bundles_install_bundles_confirm_bundles_form( $bundles, $modules, $form_values ){
  $form = array();
  $items = array();

  // Check values for submitted dependency errors.
  if ($bundle_dependencies = bundles_install_build_bundle_dependencies($bundles, $form_values)) {
    $form['validation_modules'] = array( '#type' => 'value', '#value' => $modules );
    $form['validation_bundles'] = array( '#type' => 'value', '#value' => $bundles );
    // preserve the already switched on bundles
    foreach ($dependencies as $name => $missing_dependencies) {
      $form['bundle_status'][$name] = array('#type' => 'hidden', '#value' => 1);
      foreach ($missing_dependencies as $k => $dependency) {
        $form['bundle_status'][$dependency] = array('#type' => 'hidden', '#value' => 1);
        $info = $bundles[$dependency]->info;
        $missing_dependencies[$k] = $info['name'] ? $info['name'] : drupal_ucfirst($dependency);
      }
      $t_argument = array(
        '%bundle' => $bundles[$name]->info['name'],
        '%dependencies' => implode(', ', $missing_dependencies),
      );
      $items[] = strtr(format_plural(count($missing_dependencies), 'You must enable the %dependencies bundle to install the %bundle bundle.', 'You must enable the %dependencies bundles to install the %bundle bundle.'), $t_argument);
    }

    $form['text'] = array('#value' => theme('item_list', $items));
    $form['#multistep'] = TRUE;
    $form['#action'] = url('admin/build/bundles/install/confirm');
  }

  // Set some default form values
  if($form){
    $form = confirm_form(
      $form,
      t('Some bundle dependencie(s) must be meet'),
      'admin/build/bundles/install',
      t('Would you like to continue with enabling the above?'),
      t('Continue'),
      t('Cancel'));
  }
  return $form;
}

function bundles_install_bundles_confirm_modules_form( $bundles, $modules, $form_values ){
  $form = array();
  $items = array();
  // Check values for submitted dependency errors.
  if ($module_dependencies = bundles_install_build_module_dependencies($bundles, $modules, $form_values)) {
    $form['validation_modules'] = array( '#type' => 'value', '#value' => $modules );
    $form['validation_bundles'] = array( '#type' => 'value', '#value' => $bundles );
    // preserve the already switched on bundles
    foreach ($bundles as $name => $bundle) {
      if ($bundle->status) {
        $form['bundle_status'][$name] = array('#type' => 'hidden', '#value' => 1);
      }
    }
    $form['bundle_status']['#tree'] = true;

    // preserve the already switched on modules
    foreach ($modules as $name => $module) {
      if ($module->status) {
        $form['status'][$name] = array('#type' => 'hidden', '#value' => 1);
      }
    }

    $form['status']['#tree'] = true;
    // These are the modules that depend on existing modules.
    reset($module_dependencies);
    while ( list($bundle,$module_list) = each($module_dependencies) ) {
      $missing_dependencies = array();
      $form['bundle_status'][$bundle] = array('#type' => 'hidden', '#value' => 1);
      reset($module_list);
      while( list(,$module) = each($module_list) ){
        $form['status'][$module] = array('#type' => 'hidden', '#value' => 1);
        $info = $modules[$module]->info;
        $missing_dependencies[] = $info['name'] ? $info['name'] : drupal_ucfirst($module);
      }
      $t_argument = array(
        '%bundle' => $bundles[$bundle]->info['name'],
        '%dependencies' => implode(', ', $missing_dependencies),
      );
      $items[] = strtr(format_plural(count($missing_dependencies), 'You must enable the %dependencies module to install the %bundle bundle.', 'You must enable the %dependencies modules to install the %bundle bundle.'), $t_argument);
    }
//     // Check values for dependency that we can't install.
//     if ($dependencies = system_module_build_dependencies($modules, $form_values)) {
//       // These are the modules that depend on existing modules.
//       foreach (array_keys($dependencies) as $name) {
//         $form_values['status'][$name] = array('#type' => 'hidden', '#value' => 1);
//       }
//     }
    $form['text'] = array('#value' => theme('item_list', $items));

    $form['step'] = array( '#type' => 'hidden', '#value' => 'confirm/modules' );
    $form['#multistep'] = TRUE;
    $form['#action'] = url('admin/build/bundles/install/confirm');
    $form['#theme'] = 'bundles_install_bundles_confirm_form';
  }

  if($form){
    // Set some default form values
    $form = confirm_form(
      $form,
      t('Some bundle dependencie(s) must be meet'),
      'admin/build/bundles/install',
      t('Would you like to continue with enabling the above?'),
      t('Continue'),
      t('Cancel'));
  }
  return $form;
}

function theme_bundles_install_bundles_confirm_form( $form ){
  return drupal_render($form);
}
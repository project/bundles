<?php

function bundles_uninstall_bundles_form($form_values = NULL) {
  require_once( "./includes/install.inc" );

  // Display the confirm form if any bundles have been submitted.
  if ($confirm_form = bundles_uninstall_bundles_confirm_form($form_values)) {
    return $confirm_form;
  }

  $form = array();

  // Pull all disabled bundles from the system table.
  $disabled_bundles = db_query("SELECT name, filename FROM {system} WHERE type = 'bundle' AND status = 0 AND schema_version > %d ORDER BY name", SCHEMA_UNINSTALLED);
  while ($bundle = db_fetch_object($disabled_bundles)) {

    // Grab the .info file and set name and description.
    $info = bundle_get_info_file($bundle->filename);

    // Load the .install file, and check for an uninstall hook.
    // If the hook exists, the bundle can be uninstalled.
    bundle_load_install($bundle->name);
    if (module_hook($bundle->name, 'uninstall') || $items = bundles_management_get_schema_items($bundle->name)) {
      $form['bundles'][$bundle->name]['name'] = array('#value' => $info['name'] ? $info['name'] : $bundle->name);
      $form['bundles'][$bundle->name]['description'] = array('#value' => t($info['description']));
      $options[$bundle->name] = '';
    }
  }

  // Only build the rest of the form if there are any bundles available to uninstall.
  if (count($options)) {
    $form['uninstall'] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
    );
    $form['buttons']['submit'] = array(
      '#type' => 'button',
      '#value' => t('Uninstall'),
    );
    $form['#multistep'] = TRUE;
    $form['#action'] = url('admin/build/bundles/uninstall/confirm');
  }
  return $form;
}

function bundles_uninstall_bundles_submit( $form_id, $form_values ){
  // Make sure the install API is available.
  include_once './includes/install.inc';
  bundles_load_library( "bundles.install.inc" );
  // Call the uninstall routine for each selected module.
  foreach (array_filter($form_values['uninstall']) as $bundle => $value) {
    bundles_system_bundle_uninstall($bundle);
  }
  drupal_set_message(t('The selected bundles have been uninstalled.'));

  return 'admin/build/bundles/uninstall';
}

function bundles_uninstall_bundles_confirm_form($form_values=null) {
  // Nothing to build.
  if (!isset($form_values)) {
    return;
  }

  // Construct the hidden form elements and list items.
  foreach (array_filter($form_values['uninstall']) as $bundle => $value) {
    $info = bundle_get_info_file(drupal_get_filename('bundle', $bundle));
    $uninstall[] = $info['name'];
    $form['uninstall'][$bundle] = array('#type' => 'hidden',
      '#value' => 1,
    );
  }

  // Display a confirm form if bundles have been selected.
  if (isset($uninstall)) {
    $form['uninstall']['#tree'] = TRUE;
    $form['#multistep'] = TRUE;
    $form['bundles'] = array('#value' => '<p>'. t('The following bundles will be completely uninstalled from your site, and <em>all data from these bundles will be lost</em>!') .'</p>'. theme('item_list', $uninstall));
    $form = confirm_form(
      $form,
      t('Confirm uninstall'),
      'admin/build/bundles/uninstall',
      t('Would you like to continue with uninstalling the above?'),
      t('Uninstall'),
      t('Cancel'));
    return $form;
  }
}


function theme_bundles_uninstall_bundles($form) {
  // No theming for the confirm form.
  if (isset($form['confirm'])) {
    return drupal_render($form);
  }

  // Table headers.
  $header = array(t('Uninstall'),
    t('Name'),
    t('Description'),
  );

  // Display table.
  $rows = array();
  foreach (element_children($form['bundles']) as $bundle) {
    $rows[] = array(
      array('data' => drupal_render($form['uninstall'][$bundle]), 'align' => 'center'),
      '<strong>'. drupal_render($form['bundles'][$bundle]['name']) .'</strong>',
      array('data' => drupal_render($form['bundles'][$bundle]['description']), 'class' => 'description'),
    );
  }

  // Only display table if there are bundles that can be uninstalled.
  if (!count($rows)) {
    $rows[] = array(array('data' => t('No bundles are available to uninstall.'), 'colspan' => '3', 'align' => 'center', 'class' => 'message'));
  }

  $output  = theme('table', $header, $rows);
  $output .= drupal_render($form);

  return $output;
}

<?php

function bundle_list($refresh = FALSE, $sort = FALSE, $fixed_list = NULL) {
  static $list, $sorted_list;

  if ($refresh || $fixed_list) {
    unset($sorted_list);
    $list = array();
    if ($fixed_list) {
      foreach ($fixed_list as $name => $bundle) {
        drupal_get_filename('bundle', $name, $bundle['filename']);
        $list[$name] = $name;
      }
    }
    else {
      $result = db_query("SELECT name, filename FROM {system} WHERE type = 'bundle' AND status = 1 ORDER BY weight ASC, filename ASC");
      while ($bundle = db_fetch_object($result)) {
        if (file_exists($bundle->filename)) {
          drupal_get_filename('bundle', $bundle->name, $bundle->filename);
          $list[$bundle->name] = $bundle->name;
        }
      }
    }
  }
  if ($sort) {
    if (!isset($sorted_list)) {
      $sorted_list = $list;
      ksort($sorted_list);
    }
    return $sorted_list;
  }
  return $list;
}
bundle_list(true);

function bundle_exists($bundle) {
  $list = bundle_list();
  return array_key_exists($bundle, $list);
}

function bundle_load_install($bundle) {
  bundles_load_library( "bundles.install.inc" );
  $install_file = './'. drupal_get_path('bundle', $bundle) .'/'. $bundle .'.install';
  if (is_file($install_file)) {
    include_once $install_file;
  }
}

function bundle_enable($bundle_list) {
  $invoke_bundles = array();
  foreach ($bundle_list as $bundle) {
    $existing = db_fetch_object(db_query("SELECT status FROM {system} WHERE type = 'bundle' AND name = '%s'", $bundle));
    if ($existing->status === '0') {
      db_query("UPDATE {system} SET status = 1, throttle = 0 WHERE type = 'bundle' AND name = '%s'", $bundle);
      drupal_load('bundle', $bundle);
      $invoke_bundles[] = $bundle;
    }
  }

  if (!empty($invoke_bundles)) {
    cache_clear_all('*', 'cache_menu', TRUE);
  }

  foreach ($invoke_bundles as $bundle) {
    module_invoke($bundle, 'enable');
  }
}

function bundles_get_installed_schema_version($bundle, $reset = FALSE) {
  static $versions = array();

  if ($reset) {
    $versions = array();
  }

  if (!$versions) {
    $versions = array();
    $result = db_query("SELECT name, schema_version FROM {system} WHERE type = 'bundle'");
    while ($row = db_fetch_object($result)) {
      $versions[$row->name] = $row->schema_version;
    }
  }

  return $versions[$bundle];
}

function bundle_disable($bundle_list) {
  $invoke_bundles = array();
  foreach ($bundle_list as $bundle) {
    if (bundle_exists($bundle)) {
      bundle_load_install($bundle);
      module_invoke($bundle, 'disable');
      db_query("UPDATE {system} SET status = 0, throttle = 0 WHERE type = 'bundle' AND name = '%s'", $bundle);
      $invoke_bundles[] = $bundle;
    }
  }

  if (!empty($invoke_bundles)) {
    cache_clear_all('*', 'cache_menu', TRUE);
  }
}

function bundle_get_info_file( $filename ){
  $info = array();

  if (file_exists($filename)) {
    $info = parse_ini_file($filename);
    $array_props = array( 'module dependencies', 'bundle dependencies'  );
    reset($array_props);
    while( list(,$prop) = each($array_props) ){
      if (isset($info[$prop])) {
        $info[$prop] = explode(" ", $info[$prop]);
      }
      else {
        $info[$prop] = NULL;
      }
    }
  }
  return $info;
}

function _bundle_build_dependents($bundles) {
  foreach ($bundles as $bundlename => $bundle) {
    if (isset($bundle->info['bundle dependencies']) && is_array($bundle->info['bundle dependencies'])) {
      foreach ($bundle->info['bundle dependencies'] as $dependency) {
        if (!empty($bundles[$dependency]) && is_array($files[$dependency]->info)) {
          if (!isset($bundles[$dependency]->info['bundle dependencies'])) {
            $bundles[$dependency]->info['bundle dependencies'] = array();
          }
          $bundles[$dependency]->info['bundle dependencies'][] = $bundlename;
        }
      }
    }
  }
  return $bundles;
}

function bundles_rebuild_cache() {
  // Get current list of bundles
  $bundles = drupal_system_listing('\.bundleinfo$', 'bundles', 'name', 0);

  system_get_files_database($bundles, 'bundle');

  ksort($bundles);

  foreach ($bundles as $bundlename => $bundle) {
    $bundle->info = bundle_get_info_file($bundle->filename);
    // Skip modules that don't provide info.
    if (empty($bundle->info)) {
      unset($bundles[$bundlename]);
      continue;
    }
    $bundles[$bundlename]->info = $bundle->info;

    // Update the contents of the system table:
    // TODO: We shouldn't actually need this description field anymore. Remove me next release.
    if (isset($bundle->status) || (isset($bundle->old_filename) && $bundle->old_filename != $bundle->filename)) {
      db_query("UPDATE {system} SET description = '%s', name = '%s', filename = '%s', bootstrap = %d WHERE filename = '%s'", $bundle->info['description'], $bundle->name, $bundle->filename, $bootstrap, $bundle->old_filename);
    }
    else {
      // This is a new bundle.
      db_query("INSERT INTO {system} (name, description, type, filename, status, throttle, bootstrap) VALUES ('%s', '%s', '%s', '%s', %d, %d, %d)", $bundle->name, $bundle->info['description'], 'bundle', $bundle->filename, $bundle->status, 0, 0);
    }
  }
  $bundles = _bundle_build_dependents($bundles);
  return $bundles;

}

function bundles_system_load_bundle( $name ){
  static $files = array();

  if (isset($files[$name])) {
    return TRUE;
  }

  $filename = drupal_get_filename('bundle', $name);

  if ($filename) {
    $dir = dirname($filename);
    $name = basename( $filename, ".bundleinfo" );
    $bundlefile = "./$dir/$name.bundle";
    if( file_exists($bundlefile) )
      include_once $bundlefile;
    $files[$name] = TRUE;
    return TRUE;
  }

  return FALSE;
}

function _bundles_get_all_modules(){
  $modules = drupal_system_listing('\.module$', 'modules', 'name', 0);
  system_get_files_database($modules, 'module');
  ksort($modules);
  reset($modules);
  while ( list($modulename, $module)  = each($modules)) {
    $module->info = _module_parse_info_file(dirname($module->filename) .'/'. $module->name .'.info');
    // Skip modules that don't provide info.
    if (empty($module->info)) {
      unset($modules[$modulename]);
      continue;
    }
    $modules[$modulename]->info = $module->info;
  }
  return $modules;
}
<?php 
/*
 * Copyright 2008 Jacob Henke ( jacob.henke@gmail.com http://www.jacobhenke.net )
 */

function bundle_list_data() {
  $bundles = array();

  $result = db_query("SELECT name, filename FROM {system} WHERE type = 'bundle' AND status = 1 ORDER BY weight ASC, filename ASC");
  while ($bundle = db_fetch_object($result)) {
    if (file_exists($bundle->filename)) {
      $bundle->info = bundle_get_info_file($bundle->filename);
      if ( empty($bundle->info)) continue;
      $bundles[$bundle->name] = $bundle; 
    }
  }

  $bundles = _bundle_build_dependents($bundles);
  return $bundles;
}

function bundles_disable_module_dependencies( &$form ){
    $bundles = bundle_list_data();
    reset( $bundles );
    $checked_modules = array();
    while( list(,$bundle) = each($bundles) ){
      if( is_array( $bundle->info['module dependencies'] ) ){
        reset($bundle->info['module dependencies']);
        while( list(,$module) = each($bundle->info['module dependencies']) ){
          if( !in_array($module, $checked_modules) ){
            if( isset($form['status']['#process']['system_modules_disable']) && !in_array( $module, $form['status']['#process']['system_modules_disable'][0] ) ){
              $form['status']['#process']['system_modules_disable'][0][] = $module;
              if( isset($form['description'][$module]) ){
                $form['description'][$module]['bundle_dependencies'] = array(
                  '#value' => t('This module is required by one or more bundle(s). !link to see manage bundles', array( '!link' => l(t('Click Here'),'admin/build/bundles/install') ) ),
                  '#prefix' => '<div class="admin-bundle-dependencies">',
                  '#suffix' => '</div>',
                );
              }
              $form['disabled_modules']['#value'][$module] = in_array($module, $form['status']['#default_value']);
              $checked_modules[] = $module;
            }
          }
        }
      }
    }
}
<?php

function bundles_system_bundle_uninstall($bundle) {
  bundle_load_install($bundle);
  module_invoke($bundle, 'uninstall');
  
  drupal_set_installed_schema_version($bundle, SCHEMA_UNINSTALLED);
  $schema_items = bundles_management_get_schema_items($bundle);
  $module_list = module_implements("bundle_management");
  reset( $module_list );
  while( list(,$module) = each($module_list) ){
    $function = "{$module}_bundle_management";
    
    $meta = array( 'version' => $version );
    if( array_key_exists( $module, $schema_items ) ){
      $meta['schema_items'] = $schema_items[$module];
    }
      
    $function( "uninstall", $bundle, $meta );
  }
  $info = bundle_get_info_file(drupal_get_filename('bundle', $bundle));
  if( array_key_exists( 'module dependencies', $info ) && $info['module dependencies'] ){    
    $disable_module_list = $info['module dependencies'];
    
    $modules = _bundles_get_all_modules();
    $module_dependencies = array();
    
    reset($modules);
    while( list($modulename, $module) = each($modules) ){
      if( !in_array($modulename,$disable_module_list) &&  $module->status && is_array($module->info['dependencies']) ){
        $module_dependencies = array_merge($module_dependencies,$module->info['dependencies']);
      }
    }
    
    $bundles = bundle_list();
    reset($bundles);
    while( list(,$bundle) = each($bundles) ){
      $info = bundle_get_info_file(drupal_get_filename('bundle', $bundle));
      if( is_array($info['module dependencies']) ){
        $module_dependencies = array_merge($module_dependencies,$info['module dependencies']);
      }
    }
    
    $modules_to_disable = array();
    reset($disable_module_list);
    while( list(,$module) = each($disable_module_list) ){
      if( !in_array($module,$module_dependencies) ){
        $modules_to_disable[] = $module;
      }
    }
    
    if($modules_to_disable)
      module_disable($modules_to_disable);
  }
}

bundles_load_library("bundles_views.install.inc" );
bundles_load_library("bundles_cck.install.inc" );

function bundles_set_install_error( $options ){
  static $messages = array();
  $default_options = array( 'message' => null, 'type' => "system", 'reset' => false  );
  $options = array_merge($default_options, $options);
  extract($options);

  if( $reset ){
    $messages = array();
    return;
  }

  if( !array_key_exists( $type, $messages) ){
    $messages[$type] = array();
  }

  if( !in_array( $message, $messages[$type] ) )
    $messages[$type][] = $message;

  if( !is_null($message) ){
    $return_value = $messages;
    if( $type != '*' ) $return_value[$type];
    return $return_value;
  }
}

function bundles_management_delete_schema( $module, $bundle, $index, $version = null ){
  $schema_item = bundles_management_get_schema( $module, $bundle, $index );
  $sql = "DELETE FROM {bundles_schema} "; $params = array();
  if( $schema_item === false ){
    $sql .= "WHERE `module` = '%s' AND `bundle` = '%s' AND `item_key` = '%s'";
    $params = array( $module, $bundle, $index );
  } else {
    $sql .= "WHERE `schid` = %d";
    $params = array( $schema_item['schid'] );
  }
  
  if( !is_null($version) ){ 
    $sql .= " AND `version` = '%s'";
    $params[] = $version;
  }
  
  if( !empty($sql) ){
    db_query( $sql, $params );
    return db_error();
  }
  return false;
}

function bundles_management_add_schema( $module, $bundle, $index, $version = null, $data = null ){
  $schema_item = bundles_management_get_schema( $module, $bundle, $index );
  $sql = ""; $params = array();
  
  if($data)
    $data = serialize($data);
    
  if( $schema_item === false ){
    $sql = "INSERT {bundles_schema} (`schid`,`module`, `bundle`,`item_key`, `version`, `data`) VALUES ( %d, '%s', '%s', '%s', '%s', '%s' )";
    $params = array( db_next_id( "{bundles_schema}_schid" ) , $module, $bundle, $index, $version, $data );
  }
  else {
    $sql = "UPDATE {bundles_schema} SET `version` = '%s', `data` = '%s' WHERE `schid` = %d ";
    $params = array( $version, $data, $schema_item['schid'] );
  }
  if( !empty($sql) ){
    db_query( $sql, $params );
    return db_error();
  }
  return false;
}

function bundles_management_get_schema( $module, $bundle, $index ){
  $sql = "SELECT * FROM {bundles_schema} WHERE `module` = '%s' AND `bundle` = '%s' AND `item_key` = '%s'";
  $params = array( $module, $bundle, $index );
  $query = db_query( $sql, $params );
  $item = db_fetch_array($query); 
  if( $item ){
    $item['index'] = $item['item_key'];
    unset($item['item_key']);
    if( !empty($item['data']) )
      $item['data'] = @unserialize($item['data']);
  }
  return $item;
}

function bundles_management_get_schema_items( $bundle ){
  $sql = "SELECT * FROM {bundles_schema} WHERE `bundle` = '%s' ORDER BY `module`,`item_key` ";
  $params = array( $bundle );
  $query = db_query( $sql, $params );
  $return_value = array();
  while( $item = db_fetch_array($query) ){
    $item['index'] = $item['item_key'];
    unset($item['item_key']);
    if( !empty($item['data']) )
      $item['data'] = @unserialize($item['data']);
    $return_value[$item['module']][$item['index']] = $item;
  }
  return $return_value;
}
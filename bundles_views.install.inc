<?php

function views_bundle_management( $op, $bundle, $meta = array() ){
  switch($op){
    case "install":
      $function = "{$bundle}_install_views";
      if(function_exists($function)){
        $views = $function();
        reset($views);
        while( list(,$view) = each($views) ){
          if( ($vid = __views_create_view($view)) !== false ){
            bundles_management_add_schema( 'views', $bundle, $view->name,null, $vid );
          }
        }
      }
      return false;
     break;
    case "uninstall":
      if( array_key_exists( 'schema_items' , $meta ) ){
        $schema_items = $meta['schema_items'];
        reset($schema_items);
        while( list(,$schema_item) = each($schema_items) ){
          $view = new stdClass();
          $view->vid = $schema_item['data'];
          $view->name = $schema_item['index'];
          _views_delete_view( $view );
          bundles_management_delete_schema( 'views', $bundle, $schema_item['index'] );
        } 
      }
     break;
  }
}

function __views_create_view($view){
  $tables = array_keys(_views_get_tables());
  if (!is_array($view->requires) || !array_diff($view->requires, $tables)) {
    views_load_cache();
    views_sanitize_view($view);
    $vid = _views_save_view($view);
    views_invalidate_cache();
    menu_rebuild();
    return $vid;
  }
  return false;
}